import React from 'react';
import 'antd/dist/antd.min.css';
import './theme/dark-theme.css'; // dark-theme
import Routes from './routes';
import config from './aws-exports';
import Amplify from 'aws-amplify';
Amplify.configure(config);

class App extends React.Component {
	render () {
		return <div className="App">
			<Routes />
		</div>;
	}
}

export default App;
