import React from 'react';
import { Form, Icon, Input, Button, Row, Col, notification } from 'antd';
import './css/login.css';
import { Auth } from 'aws-amplify';
import { Authenticator, SignIn } from 'aws-amplify-react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { login } from './slices/user';

class LoginForm extends React.Component {
	state = {
		user: {
			challengeName: ''
		},
		redirect: false,
		waiting: false
	};
	logIn = event => {
		event.preventDefault();
		this.props.form.validateFields(async (err, values) => {
			if (!err) {
				try {
					if (this.state.user.challengeName === '') {
						this.setState({
							waiting: true
						});
						const user = await Auth.signIn(values.username, values.password);
						this.setState({user});
						this.props.login(user.attributes.email || 'User');
					}
					
					if (this.state.user.challengeName === 'NEW_PASSWORD_REQUIRED') {

						notification.info({
							message: 'Please Reset Your Password',
							description: 'You need to reset your password in order to continue.'
						});
						if (values.newPassword === values.newPasswordConfirm && values.newPassword !== '' && values.newPasswordConfirm !== '') {
							this.setState({
								waiting: true
							});
							await Auth.completeNewPassword(this.state.user, values.newPassword);
							this.setState({
								waiting: false
							});
							window.location = this.props.referrer || 'login';
						} else {
							this.setState({
								waiting: false
							});
							notification.error({
								message: 'Passwords Do Not Match',
								description: 'The passwords you entered do not match.'
							});
						}
					} else {
						this.setState({
							waiting: true
						});
						window.location = this.props.referrer || 'login';
					}

					this.setState({
						waiting: false
					});
				} catch (err) {
					this.setState({
						waiting: false
					});

					notification.error({
						message: 'Error While Logging In',
						description: err.message
					});
				}
			}
		});
	};
	render () {
		const { getFieldDecorator } = this.props.form;
		return (this.props.authState === 'signedIn' || this.state.redirect) ? <Redirect to={`${this.props.referrer && this.props.referrer !== '' ? this.props.referrer : '/dashboard'}`} /> : <div className="loginForm">
			<Row type="flex" justify="center" align="middle">
				<Col>
					<Form onSubmit={this.logIn} className="login-form">
						{
							this.state.user.challengeName !== 'NEW_PASSWORD_REQUIRED' && <Form.Item>
								{
									getFieldDecorator('username', { rules: [{ required: true }] })(<Input prefix={<Icon type="user" />} placeholder="Username" />)
								}
							</Form.Item>
						}
						{
							this.state.user.challengeName !== 'NEW_PASSWORD_REQUIRED' && <Form.Item>
								{
									getFieldDecorator('password', { rules: [{ required: true}] })(<Input prefix={<Icon type="lock" />} type="password" placeholder="Password" />)
								}
							</Form.Item>
						}
						{
							this.state.user.challengeName === 'NEW_PASSWORD_REQUIRED' && <Form.Item>
								{
									getFieldDecorator('newPassword', {rules: [{ required: true}] })(<Input prefix={<Icon type="lock" />} type="password" placeholder="New Password" />)
								}
							</Form.Item>
						}
						{
							this.state.user.challengeName === 'NEW_PASSWORD_REQUIRED' && <Form.Item>
								{
									getFieldDecorator('newPasswordConfirm', {rules: [{ required: true}] })(<Input prefix={<Icon type="lock" />} type="password" placeholder="Confirm New Password" />)
								}
							</Form.Item>
						}
						<Form.Item>
							{
								// getFieldDecorator('remember', {valuePropName: 'checked', initialValue: true})(<Checkbox>Remember me</Checkbox>)
							}
							<Button type="primary" htmlType="submit" className="login-form-button" loading={this.state.waiting}>
								{`${this.state.user.challengeName === 'NEW_PASSWORD_REQUIRED' ? 'Reset Password & Log In' : 'Log In'}`}
							</Button>
						</Form.Item>
					</Form>
				</Col>
			</Row>
		</div>
	}
}

LoginForm = Form.create({ name: 'login' })(LoginForm);

const mapStateToProps = (state, ownProps) => ({
});

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		login: username => dispatch(login(username)),
	};
};

LoginForm = connect(
	mapStateToProps,
	mapDispatchToProps
)(LoginForm);

class Login extends React.Component {
	render() {
		return (
			<Authenticator hide={[SignIn]}>
				<LoginForm />
			</Authenticator>
		);
	}
}

export default Login;
