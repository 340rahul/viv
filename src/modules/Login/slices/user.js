import { createSlice } from 'redux-starter-kit';
import lscache from 'lscache';

export const user = createSlice({
	slice: 'user',
	initialState: { username: null },
	reducers: {
		login: (state, action) => {
			lscache.set('user', {username: action.payload});
			return {
				username: action.payload
			};
		}
	}
});

const {actions, reducer} = user;

export const { login } = actions;
export default reducer;