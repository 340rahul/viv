import React from 'react';
import { Layout, Menu, Icon, notification } from 'antd';
import './css/dashboard.css';
import Counter from '../Counter/Counter';
import { Auth } from 'aws-amplify';
import { connect } from 'react-redux';
import lscache from 'lscache';
import { isMobile } from '../../utils';

const { Sider } = Layout;
const { SubMenu } = Menu;

class Dashboard extends React.Component {
	state = {
		collapsed: isMobile(),
		key: 1,
		isMobile: isMobile()
	};

	onCollapse = event => {
		this.setState({collapsed: !this.state.collapsed});
	}

	changeKey = key => {
		if (key <= 0) {
			this.setState({
				key: 1,
				collapsed: true
			});
		} else {
			this.setState({
				key,
				collapsed: true
			});
		}
	}

	logout = event => {
		Auth.signOut().then(data => { lscache.set('user', {username: null}) })
			.catch(error => { 
				console.log(error.message);
				notification.error(error.message);
			 });
		window.location = '/login';
	}

	render () {
		const { key } = this.state;
		const user = lscache.get('user') || this.props.user;
		let show = null;
		switch (key) {
			case 1: {
				show = <Counter/>;
				break;
			}
			case 2: {
				show = <div>Events!</div>;
				break;
			}
			default: 
				show = <Counter />;
		}

		return <div className="viv-dashboard">
			<Layout style={{ minHeight: '100vh' }}>
				<Sider className="viv-sider" collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse} collapsedWidth={this.state.isMobile ? 0 : 80}>
					<Icon className="logo" type="smile" theme="twoTone" />
					<Menu defaultSelectedKeys={['1']} mode="inline">
						<Menu.Item key="1" onClick={event => this.changeKey(1)}>
							<Icon type="control" />
							<span>Counter</span>
						</Menu.Item>
						<Menu.Item key="2" onClick={event => this.changeKey(2)}>
							<Icon type="calendar" />
							<span>Events</span>
						</Menu.Item>
						<SubMenu key="user-sub" title={<span><Icon type="user" /><span>{user.username}</span></span>}>
							<Menu.Item key="logout" onClick={this.logout}>
								<Icon type="logout" />
								<span>Log Out</span>
							</Menu.Item>
						</SubMenu>
					</Menu>
				</Sider>
				<Layout className="dashboard-content">
					{show}
				</Layout>
			</Layout>
		</div>;
	}
}

const mapStateToProps = (state, ownProps) => ({
	user: state.user
});

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Dashboard);

