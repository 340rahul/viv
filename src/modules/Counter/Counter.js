import React from 'react';
import { connect } from 'react-redux';
import { Button } from 'antd';
import {incCounter, decCounter} from './slices/counter';

class Counter extends React.Component {
	render () {
		return <div>
			<Button type="primary" onClick={event => this.props.incCounter()}>Increment</Button>
			<Button type="danger" onClick={event => this.props.decCounter()}>Decrement</Button>
			Counter: {this.props.counter}
		</div>;
	}
}

const mapStateToProps = (state, ownProps) => ({
	counter: state.counter
});

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		incCounter: () => dispatch(incCounter()),
		decCounter: () => dispatch(decCounter()),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Counter);