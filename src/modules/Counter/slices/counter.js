import { createSlice } from 'redux-starter-kit';

export const counter = createSlice({
	slice: 'counter',
	initialState: 0,
	reducers: {
		incCounter: (state, action) => {
			return state += 1;
		},
		decCounter: (state, action) => {
			return state -= 1;
		}
	}
});

const {actions, reducer} = counter;

export const {incCounter, decCounter} = actions;
export default reducer;