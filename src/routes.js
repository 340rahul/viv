import React from 'react';
import { BrowserRouter as Router, Route, Switch, withRouter, Redirect } from 'react-router-dom';
import {Spin, Icon} from 'antd';
import { Auth } from 'aws-amplify';

// Dynamic Imports -
const Login = React.lazy(() => import('./modules/Login/Login'));
const NotFound = React.lazy(() => import('./modules/404/NotFound'));
const Dashboard = React.lazy(() => import('./modules/Dashboard/Dashboard'));

class PrivateRoute extends React.Component {
	state = {
		loaded: false,
		isAuthenticated: false
	};

	authenticate () {
		Auth.currentAuthenticatedUser()
			.then (user => {
				this.setState({
					loaded: true,
					isAuthenticated: true
				});
			})
			.catch (err => {
				this.props.history.push('/login')
			});
	}

	componentDidMount () {
		this.authenticate();
		this.unlisten = this.props.history.listen(() => {
			Auth.currentAuthenticatedUser()
				.then(user => {})
				.catch(() => {
					if (this.state.isAuthenticated) {
						this.setState({
							isAuthenticated: false
						});
					}
				})
		})
	}

	componentWillUnmount () {
		this.unlisten();
	}

	render () {
		const { component: Component, ...rest } = this.props;
		const { loaded, isAuthenticated } = this.state;

		if (!loaded) {
			return null;
		}

		return <Route
			{...rest}
			render={props => {
				return isAuthenticated ? (
					<Component {...props} />
				) : (
					<Redirect to={{pathname: '/auth'}} />
				)
			}}
		/>
	}
}
PrivateRoute = withRouter(PrivateRoute);

export default class Routes extends React.Component {
	render () {
		const loader = <Icon type="loading" style={{fontSize: 32}} spin />;
		const spin = <Spin indicator={loader}/>;

		return (
			<Router>
				<React.Suspense fallback={spin}>
					<Switch>
						<Route exact path="/login" component={Login}/>
						<PrivateRoute path="/dashboard" component={Dashboard} />
						<Route component={NotFound} />
					</Switch>
				</ React.Suspense>
			</Router>
		);	
	}
}