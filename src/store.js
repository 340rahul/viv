import { configureStore } from 'redux-starter-kit';
import counterReducer from './modules/Counter/slices/counter';
import userReducer from './modules/Login/slices/user';

export const store = configureStore({
	reducer: {
		counter: counterReducer,
		user: userReducer
	}
});